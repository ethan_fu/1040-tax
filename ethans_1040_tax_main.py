# importing the module
import ast
import re
from pprint import pprint, pformat
from collections import OrderedDict
from ethans_1040_tax import *

filing_statuses = ["single",
                   "married filing jointly",
                   "married filing separately",
                   "head of household",
                   "qualifying widow"]
'''
JUST FINSIHED: make filing status function and tested and made main function
NEXT TIME: find new top level code into functions and test those

start looking at line 60 for ssn

            move hard data into separate file
'''

def main():
# reading the data from the file
    try:

        with open('dictionary.txt') as f:
            data = ast.literal_eval(f.read())
    except (FileNotFoundError, SyntaxError):
        data = {}

    d = OrderedDict([
        ('filing-status', None),
        ('your-first-name-and-middle-initial', None),
        ('last-name', None),
        ('your-social-security-number', None),
        ('spouses-ssn', None),
        ('home-address', None),
        ('apt-no', None),
        ('city-town-or-post-office', None),
        ('acquire-any-financial-interest-in-any-virtual-currency', None),
        ('standard-deduction', None),
        ('wages-salaries-tips-W-2', None),
        ('federal-income-tax-withheld-from w-2', None),
        ('routing-number', None),
        ('account-number', None)
    ])

    d.update(data)

    full_name_sections = [
        "your-first-name-and-middle-initial",
        "last-name"
    ]

    full_name_section_input = [
        "First Name and Middle Initial",
        "Last Name"
    ]

    filing_status(d, filing_statuses)

    d |= confirm_list(d, full_name_sections, full_name_section_input)

    ssn(d)

    if d['filing-status'] == "married filing jointly":
        handle_married_filing_jointly(d)

    #confirm("home-address(number and street", "Home address")

    full_address_sections = ['home-address', 'city-town-or-post-office', 'apt-no']
    full_address_input = ['Home address (number and street)',
                          'city, town, or post office',
                          'apt no(Leave blank if not applicable)']

    d |= confirm_list(d, full_address_sections, full_address_input)

    '''
    if not d["apt-no"]:
        confirmed = None
        apt_no = None

        while confirmed != 'y':
            have_apartment = input("Do you have an apartment number? (Y/N)\n").lower()

            if have_apartment == 'y':
                print("What is your apartment number")
                apt_no = input()

                print("Here is what you put as your apartment number")
                print(apt_no)

                confirmed = input("Is that correct? (Y/N)\n").lower()

            else:
                apt_no = "N/A"
                confirmed = "y"

        d['apt-no'] = apt_no
    '''
    #confirm("city-town-or-post-office", "city, town, or post office")

    if not d["acquire-any-financial-interest-in-any-virtual-currency"]:
        confirmed = None

        while confirmed != 'y':
            print('''At any time during 2021, did you recieve,
                    sell, send, exchange, or otherwise acquire any
                    financial interest in any virtual currency?''')

            virtual_currency = input("Yes/No\n").lower()

            if virtual_currency in ["y", "ye"]:
                virtual_currency = "yes"
            if virtual_currency == "n":
                virtual_currency = "no"

            if virtual_currency == 'yes':
                print("You acquired/sold/held virtual currency in 2021")
                confirmed = input("Is that correct? (Y/N)\n").lower()
            else:
                print("You did not acquire/sell/hold virtual currency in 2021")
                confirmed = input("Is that correct? (Y/N)\n").lower()

        d["acquire-any-financial-interest-in-any-virtual-currency"] = virtual_currency

    if not d['standard-deduction']:
        standard_deductions = [
            "Someone can claim you as a dependent",
            "Someone can claim your spouse as a dependent",
            "Spouse itemizes on a separate return or you were a dual-status alien",
            "None of the Above"
    ]

        print("Standard deduction missing: Fill in standard deduction")
        for index, standard_deduction in enumerate(standard_deductions):
            print(index, standard_deduction)

        standard_deduction_choice = None
        while standard_deduction_choice not in range(len(standard_deductions)):
            standard_deduction_choice = input('fill in standard deduction(0-3)\n')
            if check_int(standard_deduction_choice):
                standard_deduction_choice = int(standard_deduction_choice)
                standard_deduction = standard_deductions[standard_deduction_choice]
                break
            elif standard_deduction_choice in standard_deductions:
                standard_deduction = standard_deduction_choice
                break

        d['standard-deduction'] = standard_deduction

    confirm(d, "wages-salaries-tips-W-2",
            "The box labeled wages, salaries, tips on the W-2",
            int)

    taxable_interest = 0
    ordinary_dividends = 0
    taxable_ira_distributions = 0
    taxable_pensions_annuities = 0
    taxable_social_security_benefits = 0
    capital_gain = 0
    #for now, change --> when stupid company picks up
    other_income = 0

    total_income_list = [d['wages-salaries-tips-W-2'], taxable_interest,
                         ordinary_dividends,
                         taxable_ira_distributions,
                         taxable_pensions_annuities,
                         taxable_social_security_benefits,
                         capital_gain,
                         other_income]

    total_income = sum(total_income_list)

    schedule_1_line_22 = 0
    charitable_contributions = 0

    total_adjustments_to_income = schedule_1_line_22 + charitable_contributions

    adjusted_gross_income = total_income - total_adjustments_to_income

    if d['filing-status'] in ["single", "married filing separately"]:
        standard_deduction = 12_400
    elif d['filing-status'] in ["married filing jointly", "qualifying widow"]:
        standard_deduction = 24_800
    elif d['filing-status'] in ["head of household"]:
        standard_deduction = 18_650
    else: print("you're bad")

    qualified_business_income_deduction = 0

    total_deductions = standard_deduction + qualified_business_income_deduction

    taxable_income = adjusted_gross_income - total_deductions

    other_taxes = 0

    if taxable_income <= 9_875:
        tax = taxable_income * 0.10
    elif 9_875 < taxable_income <= 40_125:
        tax = 9_875 * 0.1 + (taxable_income - 9_875) * 0.12
    elif 40_125 < taxable_income <= 85_525:
        tax = (
                9_875 * 0.1
                + (40_125 - 9_875) * 0.12
                + (taxable_income - 40_125) * 0.22
                )
    else:
        raise ValueError('never expected taxable income to be above 85,525.\n'
                         + f'it was actually {taxable_income}')

    total_tax = other_taxes + tax

    if taxable_income < 0:
        taxable_income = 0

    confirm(d, "federal-income-tax-withheld-from w-2",
            "federal income tax withheld from w-2",
            int)

    income_tax_witheld_1099 = 0
    income_tax_withheld_other_forms = 0

    total_federal_income_tax_withheld = (
        d["federal-income-tax-withheld-from w-2"] +
        income_tax_witheld_1099 +
        income_tax_withheld_other_forms)

    estimated_tax_payments_from_2019 = 0

    total_other_payments_and_refundable_credits = 0

    total_payments = (total_other_payments_and_refundable_credits +
                      total_federal_income_tax_withheld +
                      estimated_tax_payments_from_2019)

    overpaid = 0

    if total_payments > total_tax:
        overpaid = total_payments - total_tax

    #TODO: rethink when to print, when to print about overpaid/owed.
    #very repetitive right now
    '''You have overpaid 758.0
    Please enter your routing number for the refund
    routing number not found.
    Please enter routing number
    '''

    if overpaid > 0:
        print(f'You have overpaid {overpaid}')
        print("Please enter your routing number for the refund")
        confirm(d, "routing-number", "routing number")
        confirm(d, "account-number", "account number")

    total_owed = total_tax - total_payments

    for key,value in d.items():
        print(f"{key}: {value}")

    with open('dictionary.txt', 'w') as data:
        data.write(pformat(dict(d)))

    print("Line 9: ", total_income)
    print("Line 10c: ", total_adjustments_to_income)
    print("Line 11: ", adjusted_gross_income)
    print("Line 12: ", standard_deduction)
    print("Line 15: ", taxable_income)
    print("Line 18: ", total_tax)

    if overpaid > 0:
        print("TOTAL OVERPAID: ", overpaid)
    else:
        print("TOTAL OWED: ", total_owed)

if __name__ == '__main__':
    main()
