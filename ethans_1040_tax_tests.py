#! /usr/bin/env python3

import unittest
from unittest import mock
from unittest.mock import Mock
from collections import OrderedDict
import ethans_1040_tax

class TestEthans1040Methods(unittest.TestCase):

    def test_check_int(self):
        self.assertTrue(ethans_1040_tax.check_int('123'))
        self.assertFalse(ethans_1040_tax.check_int('abc'))




    @mock.patch('ethans_1040_tax.print', create = True)
    @mock.patch('ethans_1040_tax.input', create = True)
    def test_confirm(self, mocked_input, mocked_print):
        mocked_input.side_effect = ['ethan', 'y']
        big_d = {}
        ethans_1040_tax.confirm(big_d, 'first-name', 'first name')
        self.assertTrue(big_d['first-name'] == 'ethan')

        mocked_input.side_effect = ['ethan', 'n', 'danny', 'y']
        big_d = {}
        ethans_1040_tax.confirm(big_d, 'first-name', 'first name')
        self.assertTrue(big_d['first-name'] == 'danny')

        mocked_input.side_effect = ['danny', 'y']
        big_d = {'first-name': ''}
        ethans_1040_tax.confirm(big_d, 'first-name', 'first name')
        self.assertTrue(big_d['first-name'] == 'danny')

    @mock.patch('ethans_1040_tax.print', create = True)
    @mock.patch('ethans_1040_tax.input', create = True)
    def test_confirm_but_less(self, mocked_input, mocked_print):
        mocked_input.side_effect = ['Ethan R']
        big_d = {}
        result = ethans_1040_tax.confirm_but_less(big_d,
                                         'your-first-name-and-middle-initial',
                                         'first name and middle initial')
        self.assertTrue(result == 'Ethan R')

        big_d ={'your-first-name-and-middle-initial': 'Ethan R'}
        result = ethans_1040_tax.confirm_but_less(big_d,
                                         'your-first-name-and-middle-initial',
                                         'first name and middle initial')
        self.assertTrue(result == 'Ethan R')

    @mock.patch('ethans_1040_tax.print', create = True)
    @mock.patch('ethans_1040_tax.input', create = True)
    def test_confirm_list(self, mocked_input, mocked_print):
        big_d = {'a' : 'b'}
        mocked_input.side_effect = ['y']
        result = ethans_1040_tax.confirm_list(big_d, ['a'], ['noice'])
        self.assertTrue(result == {'a': 'b'})

        big_d = {'a':'b', 'c':'d'}
        mocked_input.side_effect = ['n','lol','yousuck','y']
        result = ethans_1040_tax.confirm_list(big_d, ['a','c'], ['hi','bye'])
        self.assertTrue(result == OrderedDict([('a','lol'),('c','yousuck')]))

        big_d = {'c':'d', 'a':'b'}
        mocked_input.side_effect = ['n','yousuck','lol','y']
        result = ethans_1040_tax.confirm_list(big_d, ['c', 'a'], ['hi', 'bye'])
        self.assertTrue(result == OrderedDict([('c','yousuck'),('a','lol')]))

    @mock.patch('ethans_1040_tax.print', create = True)
    @mock.patch('ethans_1040_tax.input', create = True)
    def test_filing_status(self, mocked_input, mocked_print):
        filing_statuses = ["single",
                   "married filing jointly",
                   "married filing separately",
                   "head of household",
                   "qualifying widow"]

        d = {'filing-status':'single'}
        b = d.copy()
        ethans_1040_tax.filing_status(d, filing_statuses)
        self.assertTrue(d == b)

        d = {}
        mocked_input.side_effect = ['0']
        ethans_1040_tax.filing_status(d, filing_statuses)
        self.assertTrue(d['filing-status'] == 'single')

        d = {}
        mocked_input.side_effect = ['5', '0']
        ethans_1040_tax.filing_status(d, filing_statuses)
        self.assertTrue(d['filing-status'] == 'single')

        d = {'filing-status':''}
        mocked_input.side_effect = ['0']
        ethans_1040_tax.filing_status(d, filing_statuses)
        self.assertTrue(d['filing-status'] == 'single')

    @mock.patch('ethans_1040_tax.print', create = True)
    @mock.patch('ethans_1040_tax.input', create = True)
    def test_ssn(self, mocked_input, mocked_print):
        d = {'your-social-security-number':'123-45-6789'}
        ethans_1040_tax.ssn(d)
        self.assertTrue(d == {'your-social-security-number':'123-45-6789'})

        d = {'your-social-security-number':'1236789'}
        mocked_input.side_effect = ['123-45-6789', 'y']
        ethans_1040_tax.ssn(d)
        self.assertTrue(d == {'your-social-security-number':'123-45-6789'})

        d = {'your-social-security-number':None}
        mocked_input.side_effect = ['123-45-6789', 'y']
        ethans_1040_tax.ssn(d)
        self.assertTrue(d == {'your-social-security-number':'123-45-6789'})

        d = {'your-social-security-number':'1236789'}
        mocked_input.side_effect = ['123-45-6789', 'n', '012-34-5678', 'y']
        ethans_1040_tax.ssn(d)
        self.assertTrue(d == {'your-social-security-number':'012-34-5678'})

    def test_check_ssn(self):
        x = '123-45-6789'
        result = ethans_1040_tax.check_ssn(x)
        self.assertIsNotNone(result)

        x = '12-345-678'
        result = ethans_1040_tax.check_ssn(x)
        self.assertIsNone(result)

    @mock.patch('ethans_1040_tax.print', create = True)
    @mock.patch('ethans_1040_tax.input', create = True)
    def test_handle_married_filing_jointly(self, mocked_input, mocked_print):
        d = {'filing-status':"married filing jointly", 'spouses-ssn':None}
        mocked_input.side_effect = ['bob b','y','brown','y','123-45-6789','y']
        ethans_1040_tax.handle_married_filing_jointly(d)
        self.assertTrue(d == {'filing-status':'married filing jointly',
                              'spouses-first-and-middle':'bob b',
                              'spouses-last-name':'brown',
                              'spouses-ssn':'123-45-6789'})

        d = {'filing-status':"married filing jointly",
             'spouses-ssn':'123-45-6789'}
        mocked_input.side_effect = ['bob b','y','brown','y','y']
        ethans_1040_tax.handle_married_filing_jointly(d)
        self.assertTrue(d == {'filing-status':'married filing jointly',
                              'spouses-first-and-middle':'bob b',
                              'spouses-last-name':'brown',
                              'spouses-ssn':'123-45-6789'})

        d = {'filing-status':"married filing jointly",
             'spouses-ssn':'123-45-6789'}
        mocked_input.side_effect = ['bob b','n','bob y','y','brown','y','y']
        ethans_1040_tax.handle_married_filing_jointly(d)
        self.assertTrue(d == {'filing-status':'married filing jointly',
                              'spouses-first-and-middle':'bob y',
                              'spouses-last-name':'brown',
                              'spouses-ssn':'123-45-6789'})



'''
1) test other pathways that can be taken when running confirm
2) how to get different input's each time confirm is run?
'''

if __name__ == '__main__':
    unittest.main()
