# importing the module
import ast
import re
from pprint import pprint, pformat
from collections import OrderedDict

def check_int(str):
    try:
        int(str)
        return True
    except ValueError:
        return False

def confirm(big_d, section, section_name,
            force_type=str,
            conform=lambda x:True):

    if not(section in big_d and big_d[section] and conform(big_d[section])):

        confirmed = None

        while confirmed != "y":

            print(section_name, 'not filled.')
            print('Please enter', section_name)

            prompt_input = input()

            print('Here is what you input as your', section_name)
            print(prompt_input)

            #check whether ir passes conform
            if conform(prompt_input):
                confirmed = input("Is that correct? (Y/N)\n").lower()
            else:
                print(section_name, "is invalid, try again")

        big_d[section] = force_type(prompt_input)

def confirm_but_less(big_d, section, section_name, conform = str):
    output = ""

    if section in big_d and big_d[section]:
        output = big_d[section]

    else:
        print(section_name, 'not filled.')
        print('Please enter', section_name)
        output = input()

    return output

def confirm_list(big_d, keys, prompts):
    confirm = None
    result_d = {}

    small_d = OrderedDict([])
    for section, section_name in zip(keys, prompts):
            small_d[section] = big_d[section]

    while confirm != "y":

        for section, section_name in zip(keys,prompts):

            small_d[section] = confirm_but_less(small_d,
                                                section,
                                                section_name)

        for prompt, (key,value) in zip(prompts, small_d.items()):

            print(f'Your {prompt} is {value}')

        confirm = input("Are these values correct (Y/N)? ")

        if confirm != "y":
            for key in keys:
                small_d[key] = None

    return small_d

def filing_status(d, filing_statuses):
    if 'filing-status' not in d or not d['filing-status']:

        print("Filing status missing: Fill in filing status")

        for index, filing_status in enumerate(filing_statuses):
            print(index, filing_status)

        filing_status_choice = None
        while filing_status_choice not in range(len(filing_statuses)):
            filing_status_choice = input('fill in filing-status(0-4)\n')
            if check_int(filing_status_choice):
                filing_status_choice = int(filing_status_choice)
                if filing_status_choice in range(len(filing_statuses)):
                    filing_status = filing_statuses[filing_status_choice]
                    break
                else:
                    print(filing_status_choice,
                          "is not valid, valid index's are 0-4")
            elif filing_status_choice in filing_statuses:
                filing_status = filing_status_choice
                break

        d['filing-status'] = filing_status

def ssn(d):

    confirm(d, 'your-social-security-number',
            'Your Social Security Number (xxx-xx-xxxx)',
            str, check_ssn)

def check_ssn(x):

    return re.match('^\d{3}-\d{2}-\d{4}$', x)

def handle_married_filing_jointly(d):

    confirm(d, 'spouses-first-and-middle',
            "Spouse's first name and middle initial")

    confirm(d, 'spouses-last-name',
            "Spouse's Last Name")

    confirm(d, 'spouses-ssn',
            "Spouse's social security number",
            str, check_ssn)
